package com.cygni.service;

import org.json.JSONObject;

public interface GeneralApiService {

	/**
	 * Get result as JSONObject as explained in pdf document.
	 *
	 * @param id
	 *            MusicBrainz id
	 * @return Get result as String representation of JSON as explained in pdf document.
	 * @throws Exception
	 *
	 * TODO javadoc: Use swagger
	 *
	 */
	public String getResultAsString(String id) throws Exception;

	/**
	 * Get result as JSONObject as explained in pdf document.
	 *
	 * @param id
	 *            MusicBrainz id
	 * @return Get result as JSON as explained in pdf document.
	 * @throws Exception
	 *
	 */
	public JSONObject getResultAsJSON(String id) throws Exception;
}