package com.cygni.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.annotation.RequestScope;

import com.cygni.app.Album;
import com.cygni.app.CygniResp;

/**
 * Javadoc: TODO Use swagger
 */
@RequestScope
@Component
public class GeneralApiServiceImpl implements GeneralApiService {

	private static final String MUSIC_BRAINZ_URL_START = "http://musicbrainz.org/ws/2/artist/";
	private static final String MUSIC_BRAINZ_URL_END = "?&fmt=json&inc=url-rels+release-groups";
	private static final String COVER_ART_URL_START = "http://coverartarchive.org/release-group/";
	private static final String WIKI_URL_START = "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts&exintro=true&redirects=true&titles=";

	/* A simple way to handle high load for this api */
	private static final Integer MAX_IN_PROGRESS = 10;

	/**
	 * Get result as JSONObject as explained in pdf document.
	 *
	 * @param id
	 *            MusicBrainz id
	 * @return Get result as String representation of JSON as explained in pdf document.
	 * @throws Exception
	 *
	 * TODO javadoc: use swagger
	 */
	@Override
	public String getResultAsString(String id) throws Exception {
		try {
			if (StringUtils.isEmpty(id)) {
				throw new RuntimeException("No music brainz id provided");
			}
			final CygniResp cygniResp = createAndPopulateCygniRespFromId(id);
			return cygniResp.toJSONAsString();
		} catch (RuntimeException e) {
			return e.toString();
		}
	}

	/**
	 * Get result as JSONObject as explained in pdf document.
	 *
	 * @param id
	 *            MusicBrainz id
	 * @return Get result as JSON as explained in pdf document.
	 * @throws Exception
	 *
	 */
	@Override
	public JSONObject getResultAsJSON(String id) throws Exception  {
		try {
			final CygniResp cygniResp = createAndPopulateCygniRespFromId(id);
			return cygniResp.toJSONObject();
		} catch (RuntimeException e) {
			JSONObject jsonErr = new JSONObject();
			jsonErr.put("ERROR:", e.toString());
			return jsonErr;
		}
	}

	/**
	 * Create and populate CygniResp from input id.
	 *
	 * @param id
	 *            MusicBrainz id
	 * @return CygniResp Populated CygniResp
	 * @throws Exception
	 */
	private CygniResp createAndPopulateCygniRespFromId(String id) throws Exception {
		if (CygniResp.getInProgress() < MAX_IN_PROGRESS) {

			/* 1. Set music brainz id. */
			final CygniResp cygniResp = new CygniResp(id);

			/* 2. Set Albums to cygniResp. */
			setAlbums(cygniResp);

			/* 3. Set Description to cygniResp. */
			setDescripton(cygniResp);

			return cygniResp;

		} else {
			throw new RuntimeException("High load, try again later.");
		}
	}

	/**
	 * Set cygniResp´s albums.
	 *
	 * @param cygniResp
	 * @throws Exception
	 */
	private void setAlbums(CygniResp cygniResp) throws Exception {
		if (cygniResp != null && cygniResp.getId() != null) {

			final List<Album> albumList = new ArrayList<Album>();
			final String urlMB = MUSIC_BRAINZ_URL_START + cygniResp.getId() + MUSIC_BRAINZ_URL_END;

			/* Set id and name for all albums */
			final JSONObject JSONObjectMB = getJsonFromUrl(urlMB);
			final JSONArray jsonAlbums = JSONObjectMB.getJSONArray("release-groups");
			for (int i = 0; i < jsonAlbums.length(); i++) {
				JSONObject jsonAlbum = (JSONObject) jsonAlbums.get(i);
				String title = jsonAlbum.getString("title");
				String albumId = jsonAlbum.getString("id");
				Album album = new Album(albumId);
				album.setTitle(title);
				albumList.add(album);
			}

			/* Set wiki resource for later use when getting wiki description for the cygniResp. */
			JSONArray jsonRelations = JSONObjectMB.getJSONArray("relations");
			for (int i = 0; i < jsonRelations.length(); i++) {
				JSONObject jsonRelation = (JSONObject) jsonRelations.get(i);
				if ("wikipedia".equalsIgnoreCase(jsonRelation.getString("type"))) {
					JSONObject jsonObjURL = jsonRelation.getJSONObject("url");
					String wikiUrlBand = jsonObjURL.getString("resource");
					wikiUrlBand = wikiUrlBand.substring((wikiUrlBand.lastIndexOf('/') + 1), wikiUrlBand.length());
					cygniResp.setWikiRelations(wikiUrlBand);
				}
			}

			/* Populate each album with its imageURL */
			for (int i = 0; i < albumList.size(); i++) {

				try {
					JSONObject JSONObjectCA = getJsonFromUrl((COVER_ART_URL_START + albumList.get(i).getId()));
					JSONArray imgURLs = JSONObjectCA.getJSONArray("images");
					if (imgURLs.length() > 0) {
						JSONObject JSONObjectTmp = (JSONObject) imgURLs.get(0);
						albumList.get(i).setImageURL(JSONObjectTmp.getString("image"));
					}
				} catch (Exception E) {
					// No image found, Continue w next album
					albumList.get(i).setImageURL("Not found");
					continue;
				}
			}

			cygniResp.setAlbumList(albumList);
		}
	}

	/**
	 * Sets description to the cygniResp object.
	 *
	 * @param cygniResp
	 * @throws IOException
	 * @throws JSONException
	 * @throws Exception
	 */
	private void setDescripton(CygniResp cygniResp) throws IOException, JSONException, Exception {
		if (cygniResp != null && cygniResp.getId() != null) {
			String url = WIKI_URL_START + cygniResp.getWikiRelations();
			JSONObject JSONObjectWiki = getJsonFromUrl(url);
			JSONObject JSONObjectQuery = JSONObjectWiki.getJSONObject("query");
			JSONObject JSONObjectPages = JSONObjectQuery.getJSONObject("pages");

			/* Unknown key names, use iterator. */
			for (Iterator<?> iterator = JSONObjectPages.keys(); iterator.hasNext();) {
				String key = (String) iterator.next();
				try {
					JSONObject JSONObjectDescriptionInfo = (JSONObject) JSONObjectPages.get(key);
					String desc = (String) JSONObjectDescriptionInfo.get("extract");
					//Just a quick way of getting the description text, TODO perhaps use regexp.
					desc = desc.substring((desc.lastIndexOf("-elt") + 6), desc.length());
					cygniResp.setDescription(desc);
				} catch (Exception e) {
					/* problem with description, maybe "extract" was not found so try next key */
					continue;
				}
			}
		}
	}

	/**
	 * Gets JSON object from the url provided
	 *
	 * @param urlStr
	 *            url provided
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 * @throws Exception
	 */
	private JSONObject getJsonFromUrl(String urlStr) throws IOException, JSONException, Exception {
		StringBuilder jsonStrB = new StringBuilder();

		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.connect();
		/* conn.getResponseCode()should be 200. TODO JB Exception handling. */

		Scanner sc = new Scanner(url.openStream());
		while (sc.hasNext()) {
			jsonStrB.append(sc.nextLine());
		}
		sc.close();

		return new JSONObject(jsonStrB.toString());
	}

}