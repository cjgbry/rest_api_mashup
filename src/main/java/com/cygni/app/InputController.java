package com.cygni.app;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cygni.service.GeneralApiService;

/**
 * RestController with methods for returning required data.
 * TODO javadoc: use swagger
 * test id 5b11f4ce-a62d-471e-81fc-a69a8278c7da
 */
@RestController
public class InputController {

	@Autowired
	private GeneralApiService generalApiService;

	@RequestMapping(value = "/getjson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody JSONObject getResultAsJSON(@RequestParam(value = "id", required = false) String id) throws Exception {
		return generalApiService.getResultAsJSON(id);
	}

	@RequestMapping(value = "/get", method = RequestMethod.GET, produces = "text/plain")
	public @ResponseBody String getResultAsString(@RequestParam(value = "id", required = false) String id) throws Exception {
		return generalApiService.getResultAsString(id);
	}

}