package com.cygni.app;

import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * Represents the object that will be printed as solution to the
 * task described in mashup.pdf
 * TODO javadoc: use swagger
 */
public class CygniResp implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String description;
	private List<Album> albumList;
	/* Reference variable. */
	private String wikiRelations;

	/* A simple way to handle high load. */
	private static int inProgress = 0;

	public CygniResp() {
		inProgress++;
	}

	public CygniResp(String id) {
		inProgress++;
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Album> getAlbumList() {
		return albumList;
	}

	public void setAlbumList(List<Album> albumList) {
		this.albumList = albumList;
	}

	public String getWikiRelations() {
		return wikiRelations;
	}

	public void setWikiRelations(String wikiRelations) {
		this.wikiRelations = wikiRelations;
	}

	public static int getInProgress() {
		return inProgress;
	}

	/**
	 * Creates a JSON object from the populated CygniResp object.
	 *
	 * @return
	 * @throws JSONException
	 */
	public JSONObject toJSONObject() throws JSONException {
		JSONObject jsonReturn = new JSONObject();
		jsonReturn.put("mbid", this.getId());
		jsonReturn.put("description", this.getDescription());
		JSONArray albumArray = new JSONArray();
		for (int i = 0; i < this.getAlbumList().size(); i++) {
			JSONObject album = new JSONObject();
			Album albumI = this.getAlbumList().get(i);
			album.put("title", albumI.getTitle());
			album.put("id", albumI.getId());
			album.put("image", albumI.getImageURL());
			albumArray.put(album);
		}
		jsonReturn.put("albums", albumArray);
		CygniResp.inProgress--;
		return jsonReturn;
	}

	/**
	 * Creates a JSON object representation as a string from the populated CygniResp object.
	 * @return
	 * @throws JSONException
	 */
	public String toJSONAsString() throws JSONException {
		JSONObject jsonObject = toJSONObject();

		/* Indent for easy view in browser */
		return jsonObject.toString(4);
	}

}
