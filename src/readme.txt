Rest Api 2018-08-27 

My code at:  

src\main\java\com\cygni\app
Data and controller.

src\main\java\com\cygni\service
Service layer with logic.

\doc: Javadoc

Jar file created in win10 command window with maven command: mvnw clean package

Use API:
port 9999 is set in application.properties 

Start api with: java -jar gs-rest-service-0.1.0.jar

Open web browser and let X be a music brainz id: http://localhost:9999/get?id=X
Example: http://localhost:9999/get?id=5b11f4ce-a62d-471e-81fc-a69a8278c7da

TODO improve Exception handling, javadoc, high load.